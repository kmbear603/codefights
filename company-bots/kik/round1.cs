string[] spamDetection(string[][] messages, string[] spamSignals) {
    return new string[]{
        check1(messages),
        check2(messages),
        check3(messages),
        check4(messages, spamSignals)
    };
}

string preprocessMessage(string str){
    string msg = "";
    foreach (char c in str){
        if ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z'))
            msg += c;
        else
            msg += " ";
    }
    return msg;
}

// more than 90 % of all messages had fewer than 5 words (here, a word is defined as a sequence of consecutive Latin letters which is neither preceded nor followed by a Latin letter);
string check1(string[][] messages){
    int count = 0, total = 0;
    foreach (string[] batch in messages){
        string msg = preprocessMessage(batch[0]);
        if (msg.Split(new string[]{" "}, StringSplitOptions.RemoveEmptyEntries).Length < 5)
            count++;
        total++;
    }
    
    if ((float)count / total <= 0.9f)
        return "passed";
    else {
        // reduce fraction
        int tmp = 2;
        while (true){
            if (count % tmp == 0 && total % tmp == 0){
                count /= tmp;
                total /= tmp;
                tmp = 2;
            }
            else {
                tmp++;
                if (tmp > count)
                    break;
            }
        }

        return "failed: " + count + "/" + total;
    }
}

// more than 50 % of messages to any one user had the same content, assuming that there were at least 2 messages to that user;
string check2(string[][] messages){
    SortedDictionary<string, Dictionary<string, int>> table = new SortedDictionary<string, Dictionary<string, int>>();
    foreach (string[] batch in messages){
        string msg = batch[0];
        string recipient = batch[1];
        
        Dictionary<string, int> sub_table;
        if (table.ContainsKey(recipient))
            sub_table = table[recipient];
        else {
            sub_table = new Dictionary<string, int>();
            table.Add(recipient, sub_table);
        }
        
        int count = 0;
        if (sub_table.ContainsKey(msg)){
            count = sub_table[msg];
            sub_table.Remove(msg);
        }
        sub_table.Add(msg, count + 1);
    }
    
    List<string> failed_id = new List<string>();
    foreach (string recipient in table.Keys){
        Dictionary<string, int> sub_table = table[recipient];
        int total = 0, max = 0;
        foreach (string msg in sub_table.Keys){
            int count = sub_table[msg];
            if (count > max)
                max = count;
            total += count;
        }
        if (total > 1 && (float)max / total > 0.5f)
            failed_id.Add(recipient);
    }
    
    failed_id.Sort(delegate(string i1, string i2){
        return int.Parse(i1) - int.Parse(i2);
    });
    
    if (failed_id.Count > 0)
        return "failed: " + string.Join(" ", failed_id.ToArray());
    else
        return "passed";
}

// more than 50 % of all messages had the same content, assuming that there were at least 2 messages;
string check3(string[][] messages){
    int total = 0;
    Dictionary<string, int> table = new Dictionary<string, int>();
    foreach (string[] batch in messages){
        string msg = batch[0];
        int count = 0;
        if (table.ContainsKey(msg)){
            count = table[msg];
            table.Remove(msg);
        }
        table.Add(msg, count + 1);
        total++;
    }
    
    List<string> failed = new List<string>();
    foreach (string msg in table.Keys){
        if (total > 1 && (float)table[msg] / total > 0.5f)
            failed.Add(msg);
    }
    if (failed.Count > 0)
        return "failed: " + string.Join(" ", failed.ToArray());
    
    return "passed";
}

// more than 50 % of all messages contained at least one of the words from the given list of spamSignals (the letters' case doesn't matter).
string check4(string[][] messages, string[] spamSignals){
    Dictionary<string, string> original_spam = new Dictionary<string, string>();
    Dictionary<string, int> spam_table = new Dictionary<string, int>();
    
    foreach (string spam in spamSignals){
        spam_table.Add(spam.ToLower(), 0);
        original_spam.Add(spam.ToLower(), spam);
    }

    int match_count = 0, total = 0;
    foreach (string[] batch in messages){
        string tmp_msg = preprocessMessage(batch[0]);
        string msg = "";
        foreach (char c in tmp_msg){
            if ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || (c >= '0' && c <= '9') || c == ' ')
                msg += c;
        }

        string[] words = msg.Split(new string[]{ " " }, StringSplitOptions .RemoveEmptyEntries);
        bool match = false;
        Dictionary<string, int> this_counting = new Dictionary<string, int>();
        foreach (string word in words){
            if (spam_table.ContainsKey(word.ToLower())
                && !this_counting.ContainsKey(word.ToLower())
           ){
                match = true;
                int count = spam_table[word.ToLower()];
                spam_table.Remove(word.ToLower());
                spam_table.Add(word.ToLower(), count + 1);
                this_counting.Add(word.ToLower(), 0);
            }
        }
        if (match)
            match_count++;
        total++;
    }
    
    if ((float)match_count / total > 0.5f){
        List<string> x = new List<string>();
        foreach (string spam in spam_table.Keys){
            if (spam_table[spam] > 0)
                x.Add(original_spam[spam]);
        }
        x.Sort();
        return "failed: " + string.Join(" ", x.ToArray());
    }
    return "passed";
}