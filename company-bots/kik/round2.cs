int[][][] kikCode(string userId) {
    string bin_user_id = getBinUserId(userId);
    
    List<int[][]> all = new List<int[][]>();
    all.AddRange(transform(bin_user_id.Substring(0, 3)));
    all.AddRange(transform(bin_user_id.Substring(3, 4)));
    all.AddRange(transform(bin_user_id.Substring(7, 8)));
    all.AddRange(transform(bin_user_id.Substring(15, 10)));
    all.AddRange(transform(bin_user_id.Substring(25, 12)));
    all.AddRange(transform(bin_user_id.Substring(37, 15)));
    
    return all.ToArray();
}

string getBinUserId(string user_id){
    long val = long.Parse(user_id);
    char[] a = Convert.ToString(val, 2).PadLeft(52, '0').ToCharArray();
    Array.Reverse(a);
    return new string(a);
}

int[][][] transform(string bits){
    int fraction_count = bits.Length;

    int circumference;
    switch (bits.Length){
        case 3: circumference = 1; break;
        case 4: circumference = 2; break;
        case 8: circumference = 3; break;
        case 10: circumference = 4; break;
        case 12: circumference = 5; break;
        case 15: circumference = 6; break;
        default: throw new Exception();
    }

    int step = 360 / fraction_count;
    List<int[]> fractions = new List<int[]>();
    for (int i = 0; i < bits.Length; i++){
        if (bits[i] == '1'){
            int start = i * step;
            int end = start + step;
            fractions.Add(new int[]{ start, end });
        }
    }
    
    // merge
    for (int i = fractions.Count - 1; i >= 1; i--){
        if (fractions[i][0] == fractions[i - 1][1]){
            fractions[i - 1][1] = fractions[i][1];
            fractions.RemoveAt(i);
        }
    }
    
    // check last fraction
    if (fractions.Count > 1
        && fractions[0][0] == 0
        && fractions[fractions.Count - 1][1] == 360
   ){
        fractions[0][0] += fractions[fractions.Count - 1][0];
        fractions[0][1] += 360;
        fractions.RemoveAt(fractions.Count - 1);
    }
    
    List<int[][]> ret = new List<int[][]>();
    foreach (int[] fraction in fractions){
        ret.Add(new int[][]
        {
            new int[]{ circumference, fraction[0] },
            new int[]{ circumference, fraction[1] }
        });
    }
    
    return ret.ToArray();
}