int[] rateLimit(int[][] sentBatches, int[][] receivedMessages, int startingAllowance) {
    List<Info> transactions = new List<Info>();
    AppendTransaction<SentInfo>(transactions, sentBatches);
    AppendTransaction<ReceivedInfo>(transactions, receivedMessages);
    
    // initialize allowance
    Dictionary<int, int> allowance_table = new Dictionary<int, int>();
    foreach (Info info in transactions){
        foreach (int user in info.userId){
            if (allowance_table.ContainsKey(user))
                continue;
            allowance_table.Add(user, startingAllowance);
        }
    }

    int index = 0;
    List<int> failed_batch = new List<int>();
    DateTime last_date = DateTime.MinValue;
    foreach (Info info in transactions){
        if (info.time.Date != last_date){
            ResetAllowance(allowance_table, startingAllowance);
            last_date = info.time.Date;
        }
        
        if (info is SentInfo){
            // check allowance
            bool failed;
            if (allowance_table.Count == 0 || info.userId.Length == 0)
                failed = true;
            else {
                failed = false;
                foreach (int user in info.userId){
                    int allowance = allowance_table[user];
                    if (allowance == 0)
                        failed = true;
                }
            }
            
            if (failed)
                failed_batch.Add(index);
            else
                DecrementAllowance(allowance_table, info.userId);
            
            index++;
        }
        else {
            int original = allowance_table[info.userId[0]];
            allowance_table.Remove(info.userId[0]);
            allowance_table.Add(info.userId[0], original + 1);
        }
    }
    
    return failed_batch.ToArray();
}

void DecrementAllowance(Dictionary<int, int> allowance_table, int[] users){
    foreach (int user in users){
        int original = allowance_table[user];
        allowance_table.Remove(user);
        allowance_table.Add(user, original - 1);
    }
}

void ResetAllowance(Dictionary<int, int> allowance_table, int allowance){
    int[] all_users = allowance_table.Keys.ToArray();
    allowance_table.Clear();
    foreach (int user in all_users)
        allowance_table.Add(user, allowance);
}

void AppendTransaction<T>(List<Info> transactions, int[][] raw_infos)
    where T : Info, new()
{
    foreach (int[] r in raw_infos){
        int unix_time = r[0];
        int[] users = new int[r.Length - 1];
        for (int i = 1; i < r.Length; i++)
            users[i - 1] = r[i];
        
        T info = new T(){
            time = new DateTime(1970, 1, 1) + TimeSpan.FromSeconds(unix_time),
            userId = users,
            index = transactions.Count
        };
        
        int start = 0, end = transactions.Count - 1;
        while (start <= end){
            int mid = (start + end) / 2;
            if (Info.Compare(info, transactions[mid]) <= 0)
                end = mid - 1;
            else
                start = start + 1;
        }
        transactions.Insert(start, info);
    }
}

class Info {
    public int index;
    public DateTime time;
    public int[] userId;
    
    public static int Compare(Info i1, Info i2){
        if (i1.time < i2.time)
            return -1;
        else if (i1.time > i2.time)
            return 1;
        else if (i1 is ReceivedInfo && i2 is SentInfo)
            return -1;
        else if (i1 is SentInfo && i2 is ReceivedInfo)
            return 1;
        else
            return i1.index - i2.index;
    }
}

class SentInfo : Info {
    public SentInfo(){
        
    }
}

class ReceivedInfo : Info {
    public ReceivedInfo(){
    }
}