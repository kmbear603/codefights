string[][] dailyOHLC(int[] timestamp, string[] instrument, string[] side, double[] price, int[] size) {
    List<Info> table = new List<Info>();
    
    for (int i = 0; i < instrument.Length; i++){
        DateTime time = unixTimeToDateTime(timestamp[i]);
        Info info = table.Find(x => {
            return x.instrument == instrument[i] && x.time.Date == time.Date;
        });
        if (info == null){
            info = new Info();
            info.instrument = instrument[i];
            info.time = time;
            info.open = info.high = info.low = price[i];
            table.Add(info);
        }
        if (info.high < price[i])
            info.high = price[i];
        if (info.low > price[i])
            info.low = price[i];
        info.close = price[i];
    }
    
    table.Sort(delegate(Info i1, Info i2){
        if (i1.time.Date < i2.time.Date)
            return -1;
        else if (i1.time.Date > i2.time.Date)
            return 1;
        else
            return string.Compare(i1.instrument, i2.instrument);
    });
    
    return (from info in table select new string[]{
        info.time.ToString("yyyy-MM-dd"),
        info.instrument,
        info.open.ToString("f2"),
        info.high.ToString("f2"),
        info.low.ToString("f2"),
        info.close.ToString("f2")
    }).ToArray();
}

class Info {
    public string instrument;
    public DateTime time;
    public double open;
    public double high;
    public double low;
    public double close;
}

DateTime unixTimeToDateTime(int ut){
    return new DateTime(1970, 1, 1) + TimeSpan.FromSeconds(ut);
}