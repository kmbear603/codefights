int[][] serverFarm(int[] jobs, int servers) {
    // sort by descending order
    int[] sorted_index = Enumerable.Range(0, jobs.Length).ToArray<int>();
    Array.Sort(sorted_index, delegate(int i1, int i2){
        if (jobs[i2] != jobs[i1])
           return jobs[i2] - jobs[i1];
        else
           return i1 - i2;
    });
    
    int[] next_available = new int[servers];
    
    List<int>[] table = new List<int>[servers];
    foreach (int ji in sorted_index){
        int min_i = -1; // find the earliest available server
        for (int i = 0; i < servers; i++){
            if (min_i == -1 || next_available[i] < next_available[min_i])
                min_i = i;
        }
        if (table[min_i] == null)
            table[min_i] = new List<int>();
        table[min_i].Add(ji);
        next_available[min_i] += jobs[ji];
    }
    
    List<int[]> ret = new List<int[]>();
    foreach (List<int> l in table){
        int[] arr;
        if (l == null)
            arr = new int[0];
        else {
            arr = l.ToArray();
            //Array.Sort(arr);
        }
        ret.Add(arr);
    }

    return ret.ToArray();
}
