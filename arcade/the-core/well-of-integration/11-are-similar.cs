bool areSimilar(int[] A, int[] B) {
    int a = 0, b = 0;
    for (int i = 0; i < A.Length; i++){
        if (A[i] == B[i])
            continue;
        if (a == 0){
            a = A[i];
            b = B[i];
        }
        else  {
            if (a != B[i] || b != A[i])
                return false;
            a = 0;
        }
    }
    return a == 0;
}

