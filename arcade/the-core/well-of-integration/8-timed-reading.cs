int timedReading(int maxLength, string text) {
    Dictionary<char, bool> char_table = new Dictionary<char, bool>();
    for (char c = 'a'; c <= 'z'; c++)
        char_table.Add(c, true);
    
    text = text.ToLower();
    
    string new_text = "";
    foreach (char c in text){
        if (c != ' ' && !char_table.ContainsKey(c))
            continue;
        new_text += c;
    }
    
    string[] words = new_text.Split(new char[]{' '}, StringSplitOptions.RemoveEmptyEntries);
    
    int count = 0;
    foreach (string word in words){
        if (word.Length <= maxLength)
            count++;
    }
    
    return count;
}

