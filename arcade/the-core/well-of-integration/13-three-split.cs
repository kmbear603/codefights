int threeSplit(int[] a) {
    int all_sum = 0;
    int[] sum_from_beginning = new int[a.Length];
    for (int i = 0; i < a.Length; i++){
        all_sum += a[i];
        if (i == 0)
            sum_from_beginning[i] = a[i];
        else
            sum_from_beginning[i] = sum_from_beginning[i - 1] + a[i];
    }
    
    int count = 0;
    for (int i = 1; i < a.Length - 1; i++){
        int sum1 = sum_from_beginning[i] - a[i];
        if (sum1 * 3 != all_sum)
            continue;
        for (int j = i + 1; j < a.Length; j++){
            int sum2 = sum_from_beginning[j] - a[j] - sum1;
            if (sum1 == sum2)
                count++;
        }
    }
    
    return count;
}

