int houseNumbersSum(int[] inputArray) {
    int total = 0;
    foreach (int i in inputArray){
        if (i == 0)
            break;
        total += i;
    }
    return total;
}
