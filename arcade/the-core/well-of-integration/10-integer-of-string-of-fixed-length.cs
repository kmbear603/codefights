string integerToStringOfFixedWidth(int number, int width) {
    string str = number.ToString();
    return str.Length > width ? str.Substring(str.Length - width) : str.PadLeft(width, '0');
}

