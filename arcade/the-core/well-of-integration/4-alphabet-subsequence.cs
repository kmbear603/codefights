bool alphabetSubsequence(string s) {
    for (int i = 1; i < s.Length; i++){
        if (s[i] > s[i - 1])
            continue;
        return false;
    }
    return true;
}
