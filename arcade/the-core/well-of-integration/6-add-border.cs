string[] addBorder(string[] picture) {
    List<string> ret = new List<string>();
    int width = picture[0].Length + 2;
    ret.Add("".PadLeft(width, '*'));
    foreach (string line in picture)
        ret.Add("*" + line + "*");
    ret.Add("".PadLeft(width, '*'));
    return ret.ToArray();
}
