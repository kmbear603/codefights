bool adaNumber(string line) {
    line = line.Replace("_", "").ToLower();
    if (line.IndexOf("#") == -1){
        return checkDigits(line, 10);
    }
    else {
        int p1 = line.IndexOf("#");
        int b;
        if (!int.TryParse(line.Substring(0, p1), out b))
            return false;
        if (b < 2 || b > 16)
            return false;
        int p2 = line.IndexOf("#", p1 + 1);
        if (p2 == -1)
            return false;
        if (line.Substring(p2 + 1).IndexOf("#") != -1)
            return false; // make sure no more "#"
        return checkDigits(line.Substring(p1 + 1, p2 - p1 - 1), b);
    }
}

bool checkDigits(string s, int b){
    if (s == "")
        return false;
    foreach (char c in s){
        if (!((c >= '0' && c <= '9') || (c >= 'a' && c <= 'f')))
            return false;
            
        int i = (c >= '0' && c <= '9') ? c - '0' : 10 + c - 'a';
        if (i >= b)
            return false;
    }
    return true;
}