string[] allLongestStrings(string[] inputArray) {
    int longest = int.MinValue;
    foreach (string s in inputArray)
        longest = longest < s.Length ? s.Length : longest;
    
    List<string> ret = new List<string>();
    foreach (string s in inputArray){
        if (s.Length == longest)
            ret.Add(s);
    }
    
    return ret.ToArray();
}
