int[] houseOfCats(int legs) {
    List<int> ret = new List<int>();
    for (int i = 0; i <= legs / 2; i++){
        if ((legs - i * 2) % 4 == 0)
            ret.Add(i);
    }
    return ret.ToArray();
}
