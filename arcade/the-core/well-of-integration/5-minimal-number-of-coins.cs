int minimalNumberOfCoins(int[] coins, int price) {
    int count = 0;
    int cursor = coins.Length - 1;
    while (price > 0){
        if (price >= coins[cursor]){
            price -= coins[cursor];
            count++;
        }
        else
            cursor--;
    }
    return count;
}
