int electionsWinners(int[] votes, int k) {
    int max = int.MinValue;
    foreach (int v in votes)
        max = max < v ? v : max;
    
    int count = 0, max_count = 0;
    foreach (int v in votes){
        if (v + k > max)
            count++;
        if (v == max)
            max_count++;
    }
    
    return k == 0 && max_count == 1 ? 1 : count;
}

