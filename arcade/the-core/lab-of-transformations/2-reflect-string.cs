string reflectString(string inputString) {
    string ret = "";
    foreach (char c in inputString)
        ret += (char)('z' - c + 'a');
    return ret;
}
