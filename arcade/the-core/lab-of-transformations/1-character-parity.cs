string characterParity(char symbol) {
    int i;
    if (int.TryParse("" + symbol, out i))
        return i % 2 == 0 ? "even" : "odd";
    else
        return "not a digit";
}

