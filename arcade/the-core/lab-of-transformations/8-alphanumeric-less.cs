bool alphanumericLess(string s1, string s2) {
    string num_token1 = "";
    string num_token2 = "";
    
    while (s1 != "" || s2 !=""){
        string token1 = consume(ref s1);
        string token2 = consume(ref s2);
        if (token1 == "" && token2 != "")
            return true;
        else if (token1 != "" && token2 == "")
            return false;
        else if (token1 != "" && token2 != ""){
            bool num1 = token1[0] >= '0' && token1[0] <= '9';
            bool num2 = token2[0] >= '0' && token2[0] <= '9';
            if (!num1 && !num2){
                if (token1 != token2)
                    return string.Compare(token1, token2) < 0;
            }
            else if (num1 && !num2)
                return true;
            else if (!num1 && num2)
                return false;
            else {
                int v1 = int.Parse(token1);
                int v2 = int.Parse(token2);
                if (v1 < v2)
                    return true;
                else if (v1 > v2)
                    return false;
                else if (num_token1 == ""){
                    num_token1 = token1;
                    num_token2 = token2;
                }
            }
        }
    }
    
    if (num_token1 == "")
        return false;
    
    return num_token1.Length > num_token2.Length;
}

string consume(ref string s){
    if (s == "")
        return "";
    if (s[0] >= '0' && s[0] <= '9'){
        string token = "";
        while (s.Length > 0){
            if (s[0] < '0' || s[0] > '9')
                break;
            token += s[0];
            s = s.Substring(1);
        }
        return token;
    }
    else {
        string token = "" + s[0];
        s = s.Substring(1);
        return token;
    }
}