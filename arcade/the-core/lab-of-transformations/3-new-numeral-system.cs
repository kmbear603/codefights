string[] newNumeralSystem(char number) {
    List<string> ret = new List<string>();
    for (char c = 'A'; c <= (number - 'A') / 2 + 'A'; c++)
        ret.Add("" + c + " + " + (char)(number - c + 'A') +  "");
    return ret.ToArray();
}
