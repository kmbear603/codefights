bool higherVersion(string ver1, string ver2) {
    while (ver1 != "" && ver2 != ""){
        int pos1 = ver1.IndexOf('.');
        int pos2 = ver2.IndexOf('.');
        
        int v1 = int.Parse(pos1 == -1 ? ver1 : ver1.Substring(0, pos1));
        int v2 = int.Parse(pos2 == -1 ? ver2 : ver2.Substring(0, pos2));
        if (v1 != v2)
            return v1 > v2;

        if (pos1 == -1 || pos2 == -1)
            break;
        
        ver1 = ver1.Substring(pos1 + 1);
        ver2 = ver2.Substring(pos2 + 1);
    }
    return false;
}
