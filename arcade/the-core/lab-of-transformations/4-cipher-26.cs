string cipher26(string message) {
    string cipher = "" + message[0];
    int sum = 0;
    for (int i = 1; i < message.Length; i++){
        int change = message[i] - message[i - 1];
        if (change < 0)
            change += 26;
        char cipher_c = (char)('a' + change);
        sum = (sum + (cipher_c - 'a')) % 26;
        cipher += cipher_c;
    }
    return cipher;
}
