string stolenLunch(string note) {
    string ret = "";
    foreach (char c in note){
        if (c >= '0' && c <= '9')
            ret += (char)(c - '0' + 'a');
        else if (c >= 'a' && c <= 'j')
            ret += (char)(c - 'a' + '0');
        else
            ret += c;
    }
    return ret;
}
