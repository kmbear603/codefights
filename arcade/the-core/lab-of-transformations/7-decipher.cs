string decipher(string cipher) {
    string curr = "";
    string plain = "";
    foreach (char c in cipher){
        curr += c;
        int v = int.Parse(curr);
        if (v >= 'a' && v <= 'z'){
            plain += (char)v;
            curr = "";
        }
    }
    return plain;
}
