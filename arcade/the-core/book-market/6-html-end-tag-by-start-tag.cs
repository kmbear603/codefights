string htmlEndTagByStartTag(string startTag) {
    int stop = startTag.IndexOf(" ");
    if (stop == -1)
        stop = startTag.IndexOf(">");
    string name = startTag.Substring(1, stop - 1);
    return "</" + name + ">";
}
