bool isMAC48Address(string inputString) {
    string[] tokens = inputString.Split('-');
    if (tokens == null || tokens.Length != 6)
        return false;
    
    Dictionary<string, bool> table = new Dictionary<string, bool>();
    for (int i = 0; i <= 0xff; i++){
        table.Add(i.ToString("X02"), true);
    }
    
    foreach (string token in tokens){
        if (!table.ContainsKey(token))
            return false;
    }
    
    return true;
}
