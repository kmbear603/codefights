string properNounCorrection(string noun) {
    return noun.ToUpper()[0] + noun.Substring(1, noun.Length - 1).ToLower();
}
