bool isCaseInsensitivePalindrome(string inputString) {
    inputString = inputString.ToLower();
    for (int i = 0; i < inputString.Length / 2; i++){
        if (inputString[i] != inputString[inputString.Length - 1 - i])
            return false;
    }
    return true;
}
