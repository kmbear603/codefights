bool isTandemRepeat(string inputString) {
    return inputString.Length % 2 == 0
        && inputString.Substring(0, inputString.Length / 2) == inputString.Substring(inputString.Length / 2, inputString.Length / 2);
}
