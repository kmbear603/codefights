bool isUnstablePair(string filename1, string filename2) {
    int cmp1 = 0;
    for (int i = 0; i < filename1.Length && i < filename2.Length; i++){
        cmp1 = filename1[i] - filename2[i];
        if (cmp1 != 0)
            break;
    }
    if (cmp1 == 0)
        cmp1 = filename1.Length - filename2.Length;

    int cmp2 = string.Compare(filename1, filename2, true);

    return cmp1 * cmp2 <= 0;
}
