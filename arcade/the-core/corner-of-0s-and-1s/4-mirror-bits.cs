int mirrorBits(int a) {
            string s = Convert.ToString(a, 2);
            char[] c = s.ToCharArray();
            Array.Reverse(c);
            return Convert.ToInt32(new string(c),2);
}
