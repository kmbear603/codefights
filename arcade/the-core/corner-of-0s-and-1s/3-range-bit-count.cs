int rangeBitCount(int a, int b) {
    int count = 0;
    for (int i = a; i<=b; i++){
        string s = Convert.ToString(i, 2);
        count += s.Count(x=>{ return x=='1';});
    }
    return count;
}
