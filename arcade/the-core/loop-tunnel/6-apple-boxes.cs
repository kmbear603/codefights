int appleBoxes(int k) {
    int ret = 0;
    for (int i = 0; i <= k; i++){
        if (i%2==0)
            ret += i*i;
        else
            ret -= i*i;
    }
    return ret;
}
