int countBlackCells(int n, int m) {
    float slope = (float)n / m;
    
    Dictionary<int, bool> crossed = new Dictionary<int, bool>();
    
    Func<float, float, List<int>> find_crossed = delegate(float x_, float y_){
        List<int> list = new List<int>();
        
        int fx = (int)Math.Floor(x_);
        int fy = (int)Math.Floor(y_);
        if (fx < m && fy < n){
            list.Add(fy * n + fx);
        
            if (fx > 0 && x_ == Math.Floor(x_))
                list.Add(fy * n + fx - 1);

            if (fy > 0 && y_ == Math.Floor(y_))
                list.Add((fy - 1) * n + fx);
        }
        
        return list;
    };
    
    for (int x = 0; x <= m; x++){
        float y = slope * x;
        List<int> blocks = find_crossed(x, y);
        foreach (int block in blocks){
            if (crossed.ContainsKey(block))
                continue;
            crossed.Add(block, true);
        }
    }
    
    for (int y = 0; y <= n; y++){
        float x = (float)y / slope;
        List<int> blocks = find_crossed(x, y);
        foreach (int block in blocks){
            if (crossed.ContainsKey(block))
                continue;
            crossed.Add(block, true);
        }
    }
    
    return crossed.Count;
}
