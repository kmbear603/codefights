int candles(int candlesNumber, int makeNew) {
    int ret = candlesNumber;
    int leftover = candlesNumber;
    
    while (leftover >= makeNew){
        int new_candles = leftover / makeNew;
        leftover = leftover % makeNew + new_candles;
        ret += new_candles;
    }
    
    return ret;
}
