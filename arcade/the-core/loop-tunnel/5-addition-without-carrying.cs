int additionWithoutCarrying(int param1, int param2) {
    int ret = 0;
    int digit=0;
    while(param1>0||param2>0){
        ret+=(int)Math.Pow(10,digit)*((param1+param2)%10);
            param1/=10;
            param2/=10;

        digit++;
    }
    return ret;
}
