int leastFactorial(int n) {
    int m = 1;
    int k = 1;
    while (k<n){
        k*=m;
        m++;
    }
    return k;
}
