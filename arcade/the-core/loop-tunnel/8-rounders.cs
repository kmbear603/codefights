int rounders(int value) {
    int digit=0;
    bool carry=false;
    while (value>=10){
        if ((value+(carry?1:0))%10>=5){
            carry=true;
        }
        else
            carry=false;
        value/=10;
        digit++;
    }
    return (int)((carry?value+1:value)*Math.Pow(10,digit));
}
