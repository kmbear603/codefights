bool increaseNumberRoundness(int n) {
    int zero_count=0;
    bool non_zero_started=false;
    while (n>0){
        if (n%10==0){
            if (non_zero_started)
                return true;
        }
        else {
            non_zero_started=true;
        }
        n/=10;
    }
    return false;
}
