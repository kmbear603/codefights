int countSumOfTwoRepresentations2(int n, int l, int r) {
    int count=0;
    for (int i = l; i <= r; i++){
        int partner = n - i;
        if (partner>=l&&partner<=r&&partner>=i)
            count++;
    }
    return count;
}
