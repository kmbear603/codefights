int lineUp(string commands) {
    int total=0;
    int count=0;
    bool last_match=true;
    for (int i = 0; i < commands.Length; i++){
        if (commands[i]=='L'||commands[i]=='R'){
            last_match = false;
            count++;
            if (count%2==0){
                last_match=true;
                total++;
            }
        }
        if (commands[i]=='A'&&last_match==true)
            total++;
    }
    return total;
}
