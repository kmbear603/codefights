int phoneCall(int min1, int min2_10, int min11, int s) {
    int count = 0;
    if (s >= min1){
        count += 1;
        s -= min1;
    }
    if (s >= min2_10){
        int t = (int)Math.Floor((double)s/min2_10);
        if (t>9)
            t=9;
        count+=t;
        s-=min2_10*t;
    }
    if (count==10)
        count += (int)Math.Floor((double)s/min11);
    return count;
}
