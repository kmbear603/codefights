int lateRide(int n) {
    DateTime now = DateTime.Now.Date + TimeSpan.FromMinutes(n);
    return (int)(now.Hour/10)+(now.Hour%10)+(int)(now.Minute/10)+(now.Minute%10);
}
