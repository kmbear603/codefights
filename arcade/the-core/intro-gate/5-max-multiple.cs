int maxMultiple(int divisor, int bound) {
    return (int)Math.Floor((double)bound / divisor) * divisor;
}
