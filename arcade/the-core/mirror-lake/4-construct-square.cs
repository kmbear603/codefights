int constructSquare(string s) {
    Dictionary<string, int/*max*/> table = new Dictionary<string, int>();
    {
        // get the mapping of digit string (sorted) to max square value
        // "16"->16
        // "25"->25
        // "36"->36
        // "49"->49
        // "46"->64
        // "18"->81
        // "001"->100
        // ...
        
        int root = 0;
        while (true){
            int square = root * root;
            string str = square.ToString();
            if (str.Length > s.Length)
                break;

            if (str.Length == s.Length){
                // sort characters
                string sorted_s = sortStringCharacters(str);

                int curr;
                if (table.ContainsKey(sorted_s)){
                    curr = table[sorted_s];
                    table.Remove(sorted_s);
                }
                else
                    curr = int.MinValue;
                table.Add(sorted_s, curr > square ? curr : square);
            }

            root++;
        }
    }
    
    int val;
    if (!recursiveSubstituteAndCheck(s, table, out val))
        return -1;
    return val;
}

string sortStringCharacters(string str){
    char[] a = str.ToCharArray();
    Array.Sort(a, delegate(char c1, char c2){
        return c1 - c2;
    });
    return new string(a);
}

bool recursiveSubstituteAndCheck(string s, Dictionary<string, int> table, out int val){
    int max = int.MinValue;
    
    foreach (char c in s){
        if (c >= 'a' && c <= 'z'){
            for (int i = 9; i >= 0; i--){
                char sub = i.ToString()[0];
                if (s.IndexOf(sub) != -1)
                    continue;
                int tmp_val;
                if (recursiveSubstituteAndCheck(s.Replace(c, sub), table, out tmp_val))
                    max = (tmp_val > max ? tmp_val : max);
            }
            
            val = max;
            return val != int.MinValue;
        }
    }
    
    // if program can reach here, that means all english characters are replaced by digits

    string sorted_s = sortStringCharacters(s);

    if (!table.ContainsKey(sorted_s)){
        val = 0;
        return false;
    }
    val = table[sorted_s];
    return true;
}
