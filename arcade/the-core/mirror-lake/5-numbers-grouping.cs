int numbersGrouping(int[] a) {
    Dictionary<int, bool> ug = new Dictionary<int, bool>();
    foreach (int v in a){
        int g = (v - 1) / 10000 + 1;
        if (ug.ContainsKey(g))
            continue;
        ug.Add(g, true);
    }
    return ug.Count + a.Length;
}
