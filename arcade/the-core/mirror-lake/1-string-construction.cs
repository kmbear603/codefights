int stringsConstruction(string A, string B) {
    Dictionary<char, int> table = new Dictionary<char, int>();
    foreach (char c in B){
        int count;
        if (table.ContainsKey(c)){
            count = table[c];
            table.Remove(c);
        }
        else
            count = 0;
        table.Add(c, count + 1);
    }
    
    int all_count = 0;
    while (true){
        bool done = true;
        foreach (char c in A){
            if (!table.ContainsKey(c)){
                done = false;
                break;
            }
            int count = table[c];
            table.Remove(c);
            count--; // decrement one count
            if (count > 0)
                table.Add(c, count);
        }
        if (!done)
            break;
        all_count++;
    }
    
    return all_count;
}
