int mostFrequentDigitSum(int n) {
    List<int> s1 = new List<int>(); // first sequence
    s1.Add(n);
    while (n > 0){
        int sn = s(n);
        s1.Add(n - sn);
        n -= sn;
    }
    
    int[] s2 = (from v in s1.ToArray() select s(v)).ToArray();
    Dictionary<int, int> h = new Dictionary<int, int>(); // frequency table
    foreach (int v in s2){
        int c; // original count
        if (h.ContainsKey(v)){
            c = h[v];
            h.Remove(v);
        }
        else
            c = 0;
        h.Add(v, c + 1);
    }
    
    // find max
    int max_v = -1;
    int max_c = int.MinValue;
    foreach (int v in h.Keys){
        if (h[v] > max_c){
            max_v = v;
            max_c = h[v];
        }
        else if (h[v] == max_c && v > max_v)
            max_v = v;
    }
    return max_v;
}

int s(int x){
    int ds = 0; // digit sum
    while (x > 0){
        ds += x % 10;
        x /= 10;
    }
    return ds;
}