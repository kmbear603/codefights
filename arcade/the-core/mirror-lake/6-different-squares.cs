int differentSquares(int[][] matrix) {
    Dictionary<int/*eg. 1222*/, bool> u = new Dictionary<int, bool>();
    for (int r = 0; r < matrix.Length - 1; r++){
        for (int c = 0; c < matrix[0].Length - 1; c++){
            int v = matrix[r][c] * 1000 + matrix[r][c + 1] * 100 + matrix[r + 1][c] * 10 + matrix[r + 1][c + 1];
            if (u.ContainsKey(v))
                continue;
            u.Add(v, true);
        }
    }
    return u.Count;
}
