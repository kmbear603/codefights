int createAnagram(string s, string t) {
    Dictionary<char, int> table = new Dictionary<char, int>();
    foreach (char c in s){
        int count = 0;
        if (table.ContainsKey(c)){
            count = table[c];
            table.Remove(c);
        }
        table.Add(c, count + 1);
    }
    
    int replace = 0;
    foreach (char c in t){
        int count;
        if (!table.ContainsKey(c))
            replace++;
        else {
            count = table[c];
            table.Remove(c);
            count--;
            if (count > 0)
                table.Add(c, count);
        }
    }
    
    return replace;
}
