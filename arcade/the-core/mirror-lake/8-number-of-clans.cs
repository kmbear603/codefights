int numberOfClans(int[] divisors, int k) {
    // string represents divisibility of each divisor
    // eg. if divisors.Length=6, 100010 means divisible by divisors[0] and divisors[4]
    Dictionary<string, bool> divisible_table = new Dictionary<string, bool>();
    for (int i = 1; i <= k; i++){
        string s = "";
        foreach (int divisor in divisors)
            s += (i % divisor == 0 ? "1" : "0");
        if (divisible_table.ContainsKey(s))
            continue;
        divisible_table.Add(s, true);
    }
    return divisible_table.Count;
}
