bool isSubstitutionCipher(string string1, string string2) {
    Dictionary<char, char> mapping1 = new Dictionary<char, char>(),
        mapping2 = new Dictionary<char, char>();
    for (int i = 0; i < string1.Length; i++){
        char c1 = string1[i];
        char c2 = string2[i];
        if (mapping1.ContainsKey(c1)){
            if (mapping1[c1] != c2)
                return false;
        }
        else
            mapping1.Add(c1, c2);
        
        if (mapping2.ContainsKey(c2)){
            if (mapping2[c2] != c1)
                return false;
        }
        else
            mapping2.Add(c2, c1);
    }
    return true;
}
