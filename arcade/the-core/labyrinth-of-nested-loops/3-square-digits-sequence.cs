int squareDigitsSequence(int a0) {
    Dictionary<int, bool> history = new Dictionary<int, bool>();
    history.Add(a0, true);
    
    while (true){
        int new_a0 = 0;
        while (a0 > 0){
            new_a0 += (a0 % 10) * (a0 % 10);
            a0 /= 10;
        }
        if (history.ContainsKey(new_a0))
            break;
        history.Add(new_a0, true);
        a0 = new_a0;
    }
    
    return history.Count + 1;
}
