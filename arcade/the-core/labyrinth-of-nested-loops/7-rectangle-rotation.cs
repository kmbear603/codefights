int rectangleRotation(int a, int b) {
    // idea:
    // 1. get the length of each vector's projection to the 45 degree and 135 degree diagonal
    // 2. check if the length <=a and <=b respectively
    
    Func<double, double, bool> is_included = delegate(double x, double y){
        if (x == 0 && y == 0)
            return true;

        double slope, projection = Math.PI / 4;
        if (x == 0){
            slope = Math.PI / 2;
            if (y < 0)
                slope += Math.PI;
        }
        else if (y == 0){
            slope = 0;
            if (x < 0)
                slope += Math.PI;
        }
        else {
            slope = Math.Atan(y / x);
        }

        double length = Math.Sqrt(x * x + y * y);
        double dist_in_inclined_x = length * Math.Cos(slope - projection);
        double dist_in_inclined_y = length * Math.Sin(slope - projection);
        bool _ = dist_in_inclined_x >= -a / 2
                && dist_in_inclined_x <= a / 2
                && dist_in_inclined_y >= -b / 2
                && dist_in_inclined_y <= b / 2;
        return _;
    };
    
    int max = Math.Max(a,b);
    int count = 0;
    for (int test_x = -max; test_x <= max; test_x++){
        for (int test_y = -max; test_y <= max; test_y++){
            if (is_included(test_x, test_y))
                count++;
        }
    }
                                                     
    return count;
}
