int isSumOfConsecutive2(int n) {
    Func<int,int> sum_from_1 = delegate(int x){
        return (x * (x + 1)) / 2;
    };
    Func<int,int,int> sum_of_range = delegate(int x1, int x2){
        return sum_from_1(x2) - sum_from_1(x1 - 1);
    };
    
    int count = 0;
    for (int i = 1; i <= n; i++){
        for (int j = i + 1; j <= n; j++){
            int s = sum_of_range(i, j);
            if (s == n){
                count++;
                break;
            }
        }
    }
    return count;
}
