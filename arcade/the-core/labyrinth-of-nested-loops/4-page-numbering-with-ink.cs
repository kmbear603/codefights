int pagesNumberingWithInk(int current, int numberOfDigits) {
    int last = current;
    
    while (numberOfDigits > 0){
        int digits = (int)(Math.Log(last) / Math.Log(10)) + 1;
        if (numberOfDigits < digits)
            break;
        last++;
        numberOfDigits -= digits;
    }
    
    return last - 1;
}
