int crosswordFormation(string[] words){   
    List<string> options = new List<string>();
    options.AddRange(words);
    
    Dictionary<string, Dictionary<string, List<int>>> cross_table = new Dictionary<string, Dictionary<string, List<int>>>();
    
    int count = 0;
    for (int i1 = 0; i1 < options.Count; i1++){
        string w1 = words[i1];
        options.RemoveAt(i1);
        
        for (int i2 = 0; i2 < options.Count; i2++){
            string w2 = options[i2];
            options.RemoveAt(i2);
            
            for (int i3 = 0; i3 < options.Count; i3++){
                string w3 = options[i3];
                options.RemoveAt(i3);
                
                for (int i4 = 0; i4 < options.Count; i4++){
                    string w4 = options[i4];
                    options.RemoveAt(i4);
                    
                    count += checkCombination(w1, w2, w3, w4, cross_table);
                    
                    options.Insert(i4, w4);
                }
                
                options.Insert(i3, w3);
            }
            
            options.Insert(i2, w2);
        }
        
        options.Insert(i1, w1);
    }
    
    return count;
}

void findCrossPoints(string word1, string word2, Dictionary<string, Dictionary<string, List<int>>> cache, out int[] index1, out int[] index2){
    if (!cache.ContainsKey(word1) || !cache[word1].ContainsKey(word2)){
        Dictionary<char, List<int>> table1 = new Dictionary<char, List<int>>();

        for (int i = 0; i < word1.Length; i++){
            List<int> arr;
            if (table1.ContainsKey(word1[i]))
                arr = table1[word1[i]];
            else {
                arr = new List<int>();
                table1.Add(word1[i], arr);
            }
            arr.Add(i);
        }

        List<int> ret1 = new List<int>();
        List<int> ret2 = new List<int>();
        for (int i2 = 0; i2 < word2.Length; i2++){
            if (!table1.ContainsKey(word2[i2]))
                continue;
            List<int> arr = table1[word2[i2]];
            foreach (int i1 in arr){
                ret1.Add(i1);
                ret2.Add(i2);
            }
        }

        Dictionary<string, List<int>> sub_table;
        if (cache.ContainsKey(word1))
            sub_table = cache[word1];
        else {
            sub_table = new Dictionary<string, List<int>>();
            cache.Add(word1, sub_table);
        }
        List<int> list = new List<int>();
        sub_table.Add(word2, list);

        for (int i = 0; i < ret1.Count; i++){
            int v = ret1[i] * 15 + ret2[i];
            int start = 0, end = list.Count - 1;
            while (start <= end){
                int mid = (start + end) / 2;
                int cmp = 0;
                if (ret1[i] != list[mid] / 15)
                    cmp = ret1[i] - (list[mid] / 15);
                else
                    cmp = ret2[i] - (list[mid] % 15);
                if (cmp <= 0)
                    end = mid - 1;
                else
                    start = mid + 1;
            }
            list.Insert(start, v);
        }
    }
    
    {
        List<int> list = cache[word1][word2];
        index1 = new int[list.Count];
        index2 = new int[list.Count];
        for (int i = 0; i < list.Count; i++){
            index1[i] = list[i] / 15;
            index2[i] = list[i] % 15;
        }
    }
}

int checkCombination(string horizontal1, string horizontal2, string vertical1, string vertical2, Dictionary<string, Dictionary<string, List<int>>> cross_table){
    int[] cross_11_a, cross_11_b;
    findCrossPoints(horizontal1, vertical1, cross_table, out cross_11_a, out cross_11_b);
    if (cross_11_a.Length == 0 || cross_11_b.Length == 0)
        return 0;
    
    int[] cross_12_a, cross_12_b;
    findCrossPoints(horizontal1, vertical2, cross_table, out cross_12_a, out cross_12_b);
    if (cross_12_a.Length == 0 || cross_12_b.Length == 0)
        return 0;
    
    int[] cross_21_a, cross_21_b;
    findCrossPoints(horizontal2, vertical1, cross_table, out cross_21_a, out cross_21_b);
    if (cross_21_a.Length == 0 || cross_21_b.Length == 0)
        return 0;
    
    int[] cross_22_a, cross_22_b;
    findCrossPoints(horizontal2, vertical2, cross_table, out cross_22_a, out cross_22_b);
    if (cross_22_a.Length == 0 || cross_22_b.Length == 0)
        return 0;

    int count = 0;
    for (int i = 0; i < cross_11_a.Length; i++){
        int pt_11_a = cross_11_a[i];
        int pt_11_b = cross_11_b[i];
        
        for (int j = 0; j < cross_12_a.Length; j++){
            int pt_12_a = cross_12_a[j];
            int pt_12_b = cross_12_b[j];
            
            if (pt_12_a <= pt_11_a + 1) // area cannot be zero
                continue;
            
            for (int k = 0; k < cross_21_a.Length; k++){
                int pt_21_a = cross_21_a[k];
                int pt_21_b = cross_21_b[k];
                
                if (pt_21_b <= pt_11_b + 1) // area cannot be zero
                    continue;
                
                int estimate_pt_22_a = pt_12_a - pt_11_a + pt_21_a;
                int estimate_pt_22_b = pt_21_b - pt_11_b + pt_12_b;

                int start = 0, end = cross_22_a.Length - 1;
                while (start <= end){
                    int mid = (start + end) / 2;
                    int cmp = 0;
                    if (estimate_pt_22_a != cross_22_a[mid])
                        cmp = estimate_pt_22_a - cross_22_a[mid];
                    else
                        cmp = estimate_pt_22_b - cross_22_b[mid];
                    if (cmp <= 0)
                        end = mid - 1;
                    else
                        start = mid + 1;
                }
                if (start < cross_22_a.Length
                   && estimate_pt_22_a == cross_22_a[start]
                   && estimate_pt_22_b == cross_22_b[start])
                    count++;
            }
        }
    }
    
    return count;
}