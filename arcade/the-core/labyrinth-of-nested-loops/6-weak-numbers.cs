int[] weakNumbers(int n) {
    Func<int, int> number_of_divisor = delegate(int n_){
        int _ = 0;
        for (int i = 1; i <= n_; i++){
            if (n_ % i == 0)
                _++;
        }
        return _;
    };
    
    Dictionary<int, List<int>> number_of_divisor_table = new Dictionary<int, List<int>>();
    Dictionary<int, List<int>> weakness_table = new Dictionary<int, List<int>>();
    
    int max_weakness = int.MinValue;
    int max_num_of_divisor = int.MinValue;
    
    for (int i = 1; i <= n; i++){
        int num = number_of_divisor(i);
        max_num_of_divisor = Math.Max(max_num_of_divisor, num);
            
        List<int> list;
        if (number_of_divisor_table.ContainsKey(num))
            list = number_of_divisor_table[num];
        else {
            list = new List<int>();
            number_of_divisor_table.Add(num, list);
        }
        list.Add(i);
        
        int weakness = 0;
        for (int j = num + 1; j <= max_num_of_divisor; j++){
            if (number_of_divisor_table.ContainsKey(j))
                weakness += number_of_divisor_table[j].Count;
        }
        max_weakness = Math.Max(max_weakness, weakness);
        
        if (weakness_table.ContainsKey(weakness))
            list = weakness_table[weakness];
        else {
            list = new List<int>();
            weakness_table.Add(weakness, list);
        }
        list.Add(i);
    }
    
    return new int[]{
        max_weakness, weakness_table[max_weakness].Count
    };
}
