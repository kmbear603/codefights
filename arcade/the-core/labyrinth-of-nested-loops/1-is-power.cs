bool isPower(int n) {
    if (n == 1)
        return true;
    
    for (int i = 2; i < n; i++){
        int j = 2;
        while (true){
            int v = (int)Math.Pow(i, j++);
            if (v == n)
                return true;
            if (v > n)
                break;
        }
    }
    
    return false;
}
