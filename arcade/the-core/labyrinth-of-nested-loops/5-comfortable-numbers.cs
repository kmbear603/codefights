int comfortableNumbers(int L, int R) {
    Func<int, int> s = delegate(int v){
        int _ = 0;
        while (v > 0){
            _ += v % 10;
            v /= 10;
        }
        return _;
    };
    
    int count = 0;
    for (int a = L; a <= R; a++){
        int sa = s(a);
        int lowa = a - sa;
        int higha = a + sa;

        for (int b = a + 1; b <= R; b++){
            int sb = s(b);
            int lowb = b - sb;
            int highb = b + sb;
            
            if (b < lowa || b > higha || a < lowb || a > highb)
                continue;
                count++;
        }
    }
    
    return count;
}
