bool tennisSet(int score1, int score2) {
    if (Math.Min(score1, score2) >= 5)
        return (score1!=score2) && Math.Max(score1, score2) == 7;
    else 
        return (score1 != score2) && (score1 == 6 || score2 == 6);
}
