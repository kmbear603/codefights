bool isInfiniteProcess(int a, int b) {
    return (a-b)%2!=0||a>b;
}
