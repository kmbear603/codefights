bool isSmooth(int[] arr) {
    int mid = arr.Length % 2 == 0 ? arr[arr.Length / 2 - 1] + arr[arr.Length / 2] : arr[arr.Length / 2];
    return mid == arr[0] && mid == arr[arr.Length - 1];
}
