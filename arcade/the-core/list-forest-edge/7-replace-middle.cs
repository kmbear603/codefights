int[] replaceMiddle(int[] arr) {
    if (arr.Length % 2 == 0){
        int mid = arr[arr.Length / 2 - 1] + arr[arr.Length / 2];
        
        List<int> list = new List<int>();
        list.AddRange(arr);
        list.RemoveRange(arr.Length / 2 - 1, 2);
        list.Insert(arr.Length / 2 - 1, mid);
        arr = list.ToArray();
    }
    
    return arr;
}
