int[] concatenateArrays(int[] a, int[] b) {
    List<int> ret = new List<int>();
    ret.AddRange(a);
    ret.AddRange(b);
    return ret.ToArray();
}
