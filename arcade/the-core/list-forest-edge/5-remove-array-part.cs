int[] removeArrayPart(int[] inputArray, int l, int r) {
    List<int> list = new List<int>();
    list.AddRange(inputArray);
    list.RemoveRange(l, r - l + 1);
    return list.ToArray();
}
