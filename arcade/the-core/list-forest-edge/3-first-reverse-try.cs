int[] firstReverseTry(int[] arr) {
    if (arr.Length > 1){
        int tmp = arr[0];
        arr[0] = arr[arr.Length - 1];
        arr[arr.Length - 1] = tmp;
    }
    return arr;
}
