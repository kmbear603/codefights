int makeArrayConsecutive2(int[] sequence) {
    int min = int.MaxValue;
    int max = int.MinValue;
    foreach (int i in sequence){
        min = Math.Min(min, i);
        max = Math.Max(max, i);
    }
    
    Dictionary<int, bool> all = new Dictionary<int, bool>();
    for (int i = min; i <= max; i++)
        all.Add(i, true);
    
    foreach (int i in sequence){
        if (all.ContainsKey(i))
            all.Remove(i);
    }
    
    return all.Count;
}
