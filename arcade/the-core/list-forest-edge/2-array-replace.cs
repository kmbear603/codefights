int[] arrayReplace(int[] inputArray, int elemToReplace, int substitutionElem) {
    for (int i = 0; i < inputArray.Length; i++){
        if (elemToReplace == inputArray[i])
            inputArray[i] = substitutionElem;
    }
    return inputArray;
}
