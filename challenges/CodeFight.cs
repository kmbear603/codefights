string[] CodeFight(int n) {
    return Enumerable.Range(1, n).Select(
        i => i % 5 == 0 && i % 7 == 0
                  ? "CodeFight"
                  : (i % 5 == 0
                         ? "Code"
                         : (i % 7 == 0
                                ? "Fight"
                                : i.ToString()
                           )
                    )
    ).ToArray<string>();
}
