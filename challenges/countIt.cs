int countIt(int n) {
    int c = 0;
    do {
        int d = n % 10;
        if (d == 0 || d == 6 || d == 9)
            c++;
        else if (d == 8)
            c += 2;
        n /= 10;
    }
    while (n > 0);
    return c;
}

