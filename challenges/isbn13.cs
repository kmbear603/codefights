string[] ISBN13(string number) {
    number = number.Replace("-", "").Substring(0, 9);
    string formatted = number.Substring(0, 1) + "-" + number.Substring(1, 3) + "-" + number.Substring(4, 5);
    int c10 = calcCheckDigit(number);
    int c13 = calcCheckDigit("978" + number);
    return new string[]{
        "ISBN-10: " + formatted + "-" + (c10 == 10 ? "X" : c10.ToString()),
        "ISBN-13: 978-" + formatted + "-" + c13.ToString()
    };
}

int calcCheckDigit(string num){
    int l = num.Length;
    int v = 0;
    for (int i = 0; i < l; i++)
        v += (l == 9 ? (10 - i) : (i % 2 == 0 ? 1 : 3)) * int.Parse("" + num[i]);
    return l == 9 ? (11 - (v % 11)) % 11 : (10 - (v % 10)) % 10;
}