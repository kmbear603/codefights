bool isValidNumber(string s, bool n, bool c, bool d) {
    foreach (char cc in s){
        if (cc >= '0' && cc <= '9')
            continue;
        if (cc == '.'){
            if (!d)
                return false;
            continue;
        }
        if (cc == '-'){
            if (!n)
                return false;
            continue;
        }
        if (cc == ','){
            if (!c)
                return false;
            continue;
        }
        return false;
    }

    float t;
    if (!float.TryParse(s.Replace(",", ""), out t))
        return false;
    if (!n && t < 0)
        return false;
    if (!d && Math.Abs(t) - Math.Floor(Math.Abs(t)) > 0)
        return false;
    
    int[] commas = getPositions(s, ',');
    int[] dot = getPositions(s, '.');
    int ref_pos = dot.Length > 0 ? dot[0] : s.Length;
    for (int i = 0; i < commas.Length; i++){
        if (i == 0 && commas[i] > 3 + (t < 0 ? 1 : 0))
            return false;
        if (commas[i] == 0 || commas[i] == s.Length - 1)
            return false;
        if (i > 0 && commas[i] - commas[i - 1] != 4)
            return false;
        if (commas[i] > ref_pos)
            return false;
        if ((ref_pos - commas[i]) % 4 != 0)
            return false;
    }
    
    return true;
}

int[] getPositions(string s, char match){
    List<int> ret = new List<int>();
    for (int i = 0; i < s.Length; i++){
        if (s[i] == match)
            ret.Add(i);
    }
    return ret.ToArray();
}