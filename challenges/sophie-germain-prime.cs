// reverse challenge
int SophieGermainPrimes(int[] numbers) {
    Dictionary<int, bool> cache = new Dictionary<int, bool>();
    int c = 0;
    foreach (int n in numbers){
        if (isPrime(n, cache) && isPrime(2 * n + 1, cache))
            c++;
    }
    return c;
}

bool isPrime(int v, Dictionary<int, bool> c){
    if (c.ContainsKey(v))
        return c[v];
    
    bool p;
    if (v <= 1)
        p = false;
    else if (v <= 3)
        p = true;
    else if (v % 2 == 0 || v % 3 == 0)
        p = false;
    else {
        p = true;
        int i = 5;
        while (i * i <= v){
            if (v % i == 0 || v % (i + 2) == 0){
                p = false;
                break;
            }
            i += 6;
        }
    }
    c.Add(v, p);
    return p;
}