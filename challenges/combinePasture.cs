int combinePasture(int[] vertices) {
    int[] v = vertices;
    
    int l = v[0],
        r = v[2],
        t = v[3],
        b = v[1];
    
    for (int i = 4; i < v.Length; i += 4){
        l = v[i] < l ? v[i] : l;
        b = v[i + 1] < b ? v[i + 1] : b;
        r = v[i + 2] > r ? v[i + 2] : r;
        t = v[i + 3] > t ? v[i + 3] : t;
    }
    
    return (int)Math.Pow(Math.Max(r - l, t - b), 2);
}
