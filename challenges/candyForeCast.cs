string[] candyForeCast(int[] values) {
    if (values.Length < 4)
        return new string[0];
    
    int[] v = values;
    int next = v[0] + 1;
    int curr = v[1];
    
    double x = 0,
        x2 = 0,
        y = 0,
        y2 = 0,
        xy = 0,
        n = 0;
    for (int i = 0; i < v.Length; i += 2){
        int _x = v[i],
            _y = v[i + 1];
        x += _x;
        x2 += _x * _x;
        y += _y;
        y2 += _y * _y;
        xy += _x * _y;
        next = next < _x + 1 ? _x + 1 : next;
        curr = next < _x + 1 ? _y : curr;
        n++;
    }

    double a = (1 / n * xy - x / n * y / n) / (1 / n * x2 - x / n * x / n),
        b = y / n - a * x / n;
    int e = (int)(a * next + b);
    
    return new string[]{
        next.ToString(),
        e.ToString("f0"),
        a == 0 ? "stable" : (a > 0 ? "increasing" : "decreasing")
    };
}
