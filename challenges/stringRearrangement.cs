bool stringsRearrangement(string[] inputArray) {
    int[,] diff_table = new int[inputArray.Length, inputArray.Length];
    for (int i = 0; i < inputArray.Length; i++){
        for (int j = 0; j < inputArray.Length; j++)
            diff_table[i, j] = difference(inputArray[i], inputArray[j]);
    }
    
    List<int> pool = new List<int>();
    pool.AddRange(Enumerable.Range(0, inputArray.Length ).ToArray<int>());
    List<int> arr = new List<int>();
     return satisfy(diff_table, pool, arr);
}

bool satisfy(int[,] table, List<int> pool, List<int> arr){
    if (pool.Count == 0)
        return true;
    
    for (int i = 0; i < pool.Count; i++){
        int v = pool[i];
        if (arr.Count == 0 || table[arr[arr.Count - 1], v] == 1){
            pool.RemoveAt(i);
            arr.Add(v);
            if (satisfy(table, pool, arr))
                return true;
            arr.RemoveAt(arr.Count - 1);
            pool.Insert(i, v);
        }
    }
    return false;
}

int findNeighbor(int i, int start_j, int[,] table){
    for (int x = start_j; x < table.GetLength(1); x++){
        if (x == i)
            continue;
        if (table[i, x] == 1)
            return x;
    }
    return -1;
}

int difference(string s1, string s2){
    int d = 0;
    for (int i = 0; i < s1.Length; i++){
        if (s1[i] != s2[i])
            d++;
    }
    return d;
}