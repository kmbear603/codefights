string alpha4(string s) {
    // sum value for every 4 digit (where value=c-'a') then get the remainder of 4
    string r = "";
    int c = 0, v = 0;
    while (c < s.Length){
        v += s[c] - 'a';
        c++;
        if (c % 4 == 0 || c == s.Length){
            r += (v % 4).ToString();
            v = 0;
        }
    }
    return r;
}
